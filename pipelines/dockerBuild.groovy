import org.yaml.snakeyaml.Yaml

@NonCPS
def yamlParse(def yamlText) {
    new org.yaml.snakeyaml.Yaml().load(yamlText)
}

parameters {
    choices(name: 'namespace',
            choices: 'dev\nqa',
            description: 'What door do you choose?')
    text(defaultValue: '''journeysearch:
          image: latest
          service2:
                  image: latest
          service3:
                  image: latest''',
            name: 'services' )
}




node('buildnet') {

    def appRepo = '${GitUrl}'
    def bitbucketCredsId = 'bitbutket-key'
    def ecrRepo = '878033925423.dkr.ecr.eu-west-3.amazonaws.com'
    def ecrCredsId = 'ecr:eu-west-3:acex_test'

    cleanWs()
    checkout([$class: 'GitSCM',
              branches: [[name: '${GitBranch}']],
              doGenerateSubmoduleConfigurations: false,
              extensions: [
                      [$class: 'CleanCheckout']
              ],
              submoduleCfg: [],
              userRemoteConfigs: [[credentialsId: bitbucketCredsId, url: appRepo]]
    ])



    properties([
            parameters([
                    string(name: 'GitUrl', defaultValue: 'None', description: '', ),
                    string(name: 'GitBranch', defaultValue: 'None', description: '', ),
                    string(name: 'GitCommit', defaultValue: 'None', description: '', )
            ])
    ])

    shortCommit = sh(script: "git rev-parse --short HEAD", returnStdout: true).trim()
    repoName = sh(script: "git remote show origin -n |grep Fetch | sed  's/^[^/]*\\//\\//' | cut -c 2- |sed 's/.\\{4\\}\$//'", returnStdout: true).trim()

    configFileProvider([configFile(fileId: 'dockerconf', variable: 'dockerconf')]) {
        sh("mkdir -p ~/.kube && cp ${dockerconf} docker-compose.yml")
    }
    stage('test') {
        println 'Code has been cloned current brach is: '
    }





        container('buildnet') {


            sh("echo Git Url has to be ${GitUrl}, git branch is ${GitBranch}, commit sha ${GitCommit}")
            sh("sed -i 's/NAME/${ecrRepo}\\/dev\\/${repoName}/g' ./docker-compose.yml")
            sh("sed -i 's/TAG/${shortCommit}/g' ./docker-compose.yml")
            sh("ls -la")
            sh("cat ./docker-compose.yml")
            sh("docker-compose build")
            sh("mkdir ./deploy && touch ./deploy/Dockerfile")
            sh("echo FROM ${ecrRepo}/dev/${repoName}:${shortCommit} > ./deploy/Dockerfile")
            docker.withRegistry("https://${ecrRepo}", ecrCredsId) {
                dir('./deploy/') {
                    def customImage = docker.build("${ecrRepo}/dev/${repoName}:${shortCommit}")
                    customImage.push()
                    def customImageLatest = docker.build("${ecrRepo}/dev/${repoName}:latest")
                    customImageLatest.push()
                }
            }
            //sh("tail -f /dev/null")
        }


}
