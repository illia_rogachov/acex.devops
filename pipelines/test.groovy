// Deploy_Backend_v2
// Script Path: jenkins_pipelines/Deploy_Backend_v2.groovy

import org.yaml.snakeyaml.Yaml

@NonCPS
def yamlParse(def yamlText) {
    new org.yaml.snakeyaml.Yaml().load(yamlText)
}

parameters {
    choices(name: 'namespace',
            choices: 'dev\nqa',
            description: 'What door do you choose?')
    text(defaultValue: '''journeysearch:
          image: latest
          service2:
                  image: latest
          service3:
                  image: latest''',
            name: 'services' )
}

// currentBuild.description = "Deployment: ${namespace}"

node("helm")
        {
            cleanWs()
            try {


                def workspace = pwd()
                stage('Git checkout') {
                    def scmVars = checkout([
                            $class                           : 'GitSCM',
                            branches                         : [[name: '*/master']],
                            doGenerateSubmoduleConfigurations: false,
                            extensions                       : [/*[$class: 'CleanBeforeCheckout']*/],
                            submoduleCfg                     : [],
                            userRemoteConfigs                : [[credentialsId: 'bitbutket-key', url: 'git@bitbucket.org:ormlondon/acex.helm.git']]
                    ])

                    stage('test') {
                        println 'Code has been cloned current brach is: '
                        println scmVars.GIT_BRANCH
                    }
                }

                stage('Deployment') {
                    configFileProvider([configFile(fileId: 'kube-config', variable: 'kube_config')]) {
                        sh("mkdir -p ~/.kube && cp ${kube_config} ~/.kube/config")
                    }
                    configFileProvider([configFile(fileId: 'aws', variable: 'aws')]) {
                        sh("mkdir -p ~/.aws && cp ${aws} ~/.aws/credentials")
                    }
                    sh("export KUBECONFIG=~/.kube/config")
                    container('helm') {
                        //sh("helm init --upgrade && helm ls && pwd && ls")
                        sh("helm ls")
                        def context = kubeContext(namespace)
                        dir("${workspace}") {
                            sh("ls -la")
                            sh("tail -f /dev/null")
                            def servicesList = (params.services != null) ? yamlParse("${params.services}") : null
                                servicesList.each {
                                    println " ${it.key} test ${it.value.branch} "
                                    sh("helm install schemedeploy/1.0 --wait --name=${context}-schemedeploy --namespace ${context} --set prefix=${context}")
                                    containerName = sh(script: """kubectl get pod  -n ${namespace}  |grep  schemedeploy | grep Running |awk  {\'print \$1\'}""", returnStdout: true).trim()
                                    sh("kubectl get pod -n ${namespace} |grep ${containerName}")
                                  //  sh("kubectl  -n ${namespace} set image deployment ${namespace}-${it.key} ${appName}=878033925423.dkr.ecr.eu-west-3.amazonaws.com/dev/${it.key}:${it.value.image}")
                                 // sh("kubectl  -n dev set image deployment qa-acex.b2c.journey acex.b2c.journey=878033925423.dkr.ecr.eu-west-3.amazonaws.com/dev/acex.b2c.journey:latest")

                                }



                        }
                    }
                }

//    sendReportToSlack(namespace, params.services)
                currentBuild.result = "SUCCESS"

            } catch(e) {
//    sendReportToSlack(namespace, params.services, e)
                currentBuild.result = "FAILED"
                println "error: " + e
                throw e
            }
        }

@NonCPS
def kubeContext(def envName) {
    switch(envName) {
        case 'dev':
            return 'dev'
            break
        case 'qa':
            return 'qa'
            break
        case 'pre-prod':
            return 'pre-prod'
            break
        case 'prod':
            return 'prod'
            break
        case 'testings':
            println "${context}"
            return '${context}'
            break
        default:
            throw new Exception ("Environment ${envName} not found")
            break
    }
}