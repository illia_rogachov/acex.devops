// Show cluster status with namespace status with logs from POD
// http://172.30.0.204:8080/job/QA/job/Check_Cluster_status
// Repo URL: https://bitbucket.org/Femitelemedicine/devops.git

node('helm')
        {
            cleanWs()
            stage('Printing POD current status') {
                container('helm') {
                    configFileProvider([configFile(fileId: 'aws', variable: 'aws')]) {
                        sh("mkdir -p ~/.aws && cp ${aws} ~/.aws/credentials")
                    }
                    configFileProvider([configFile(fileId: 'kube-config', variable: 'kube_config')]) {
                        sh("mkdir -p ~/.kube && cp ${kube_config} ~/.kube/config")
                        println("PRINTING CURRENT STATUS OF ENV")
                        getStatus(namespace)
                        getImagesversion(namespace)


                        println("CHECK LINKS FROM SERVICE - NGINX LINKS")
                        getLinks(namespace)

                        println("SHOW SERVICE NAMES INTO NAMESPACE")
                        getServiceInternalLinks(namespace)

                    }
                }
            }

            stage('Getting Pod information') {
                def userInput = input(
                        id: 'userInput', message: 'Please enter the pod name from previous output - replace Null', parameters: [
                        [$class: 'TextParameterDefinition', defaultValue: 'Null', description: 'Name of pod', name: 'env'],
                        [$class: 'TextParameterDefinition', defaultValue: 'PodName', description: 'Print here pod name', name: 'target']
                ])
                echo("Name of pod: " + userInput['target'])


                container('helm') {
                    def var = userInput['target']
                    echo(var)

                    if (var != 'Null') {
                        println("GETTING APLICATION LOG")
                        getLogs(namespace, var)
                        println("PULLING IMAGE STATUS")
                        describePod(namespace, var)
                        println("IMAGE OF PULLED POD")
                        imagePod(namespace, var)
                      //  println("TOP NODES LA")
                      //  topNodes(namespace)
                    } else {
                        echo 'You have not chosen pod'
                    }
                }
            }
        }


@NonCPS
def getStatus(def env) {
    //def context = kubeContext(env)
    def shStatus = sh(returnStatus: true, script: "kubectl get pod  -n ${env}")
    println ("shStatus = ${shStatus}")
    if ( shStatus != 0) {
        println 'WARNING: helm chart was not found'
        return 1
    }
    return 0
}

@NonCPS
def getImagesversion(def env) {
  //  def context = kubeContext(env)
    def shStatus = sh(returnStatus: false, script: "set +x; for i in \$(kubectl -n ${env} get pod |awk {'print \$1'}| awk 'NR>1'); do kubectl -n ${env} describe pod \$i | grep 'Image:'; done")
    println ("shStatus = ${shStatus}")
    if ( shStatus != 0) {
        println 'Error'
        return 1
    }
    return 0
}

def getServiceInternalLinks(def env) {
    //def context = kubeContext(env)
    def shStatus = sh(returnStatus: true, script: "kubectl get svc  -n ${env} -o yaml |grep 'name: ${env}'")
    println ("shStatus = ${shStatus}")
    if ( shStatus != 0) {
        println 'WARNING: helm chart was not found'
        return 1
    }
    return 0
}
@NonCPS
def getNetstat(def env, pod) {
   // def context = kubeContext(env)
    def shStatus = sh(returnStatus: true, script: "kubectl -n ${env} exec ${pod} -i -t -- netstat -nlp")
    println ("shStatus = ${shStatus}")
    if ( shStatus != 0) {
        println 'WARNING: getNetstat method Error'
        return 1
    }
    return 0
}



@NonCPS
def imagePod(def env, def pod) {
   // def context = kubeContext(env)
    def shStatus = sh(returnStatus: true, script: "kubectl describe pod ${pod}  -n ${env} | grep -i Image")
    println ("shStatus = ${shStatus}")
    if ( shStatus != 0) {
        println 'WARNING: getLog Error'
        return 1
    }
    return 0
}

@NonCPS
def describePod(def env, def pod) {
   // def context = kubeContext(env)
    def shStatus = sh(returnStatus: true, script: "kubectl  describe pod ${pod}  -n ${env} | grep -i Pull")
    println ("shStatus = ${shStatus}")
    if ( shStatus != 0) {
        println 'WARNING: getLog Error'
        return 1
    }
    return 0
}

@NonCPS
def topNodes(def env) {
   // def context = kubeContext(env)
    def shStatus = sh(returnStatus: true, script: "kubectl top nodes")
    println ("shStatus = ${shStatus}")
    if ( shStatus != 0) {
        println 'WARNING: getLog Error'
        return 1
    }
    return 0
}


@NonCPS
def getLinks(def env) {
    // def context = kubeContext(env)
    def shStatus = sh(returnStatus: true, script: "kubectl get svc  -n ${env} -o yaml |grep ' prefix: /'")
    println ("shStatus = ${shStatus}")
    if ( shStatus != 0) {
        println 'WARNING: getLog Error'
        return 1
    }
    return 0
}


@NonCPS
def getLogs(def env, def pod) {
//  def context = kubeContext(env)
    def shStatus = sh(returnStatus: true, script: "kubectl logs ${pod}  -n ${env}")
    println ("shStatus = ${shStatus}")
    if ( shStatus != 0) {
        println 'WARNING: getLog Error'
        return 1
    }
    return 0
}

@NonCPS
def kubeContext(def envName) {
    switch(envName) {
        case 'dev':
            return 'dev'
            break
        case 'qa':
            return 'qa'
            break
        case 'testings':
            println "${context}"
            return '${context}'
            break
        default:
            throw new Exception ("Environment ${envName} not found")
            break
    }
}